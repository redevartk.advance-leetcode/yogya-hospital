# [Advance Leetcode] Yogya Hospital

This is old project to replicate a Yogyakarta,Indonesia hospital, using old archaic method, i don't want to qualify this as a [Project]

## Properties

* Framework 		: CodeIgniter 4.0.3
* Database			: MySQL (2020)
* Language		: Indonesia
* Programming Lang	: PHP 7.4+

## Developer Note

* Project doesn't use composer, use extract system.zip to use apps
* Some files/app/wording still use or mostly indonesia language
* I don't intend to continue the project
* App is not 100% done, there's some error that need to fix
